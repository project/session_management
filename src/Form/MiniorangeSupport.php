<?php

namespace Drupal\session_management\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\session_management\ContactAPI\MiniorangeContact;

/**
 * Support / Contact us form.
 */
class MiniorangeSupport extends FormBase {


  const SUBJECT_LINE = 'General Query for Session Management Module';

  const SANDBOX_TRIAL_URL = 'https://playground.miniorange.com/drupal.php?mo_module=session_management&drupal_version=10';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'miniorange-support';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('How can we help you?');

    $options = [
      'query' => $this->t('General Query'),
      'trial' => $this->t('Try Premium Features'),
      'feature_request' => $this->t('Request a Feature'),
    ];

    $form['query_type'] = [
      '#type' => 'select',
      '#title' => $this->t('How can we help you today?'),
      '#options' => $options,
      '#ajax' => [
        'callback' => '::supportCallbackFunction',
        'wrapper' => 'form-fieldset-wrapper',
      ],
      '#default_value' => $this->getRequest()->get('type') ?? 'query',
    ];

    $form['user_email'] = [
      '#type' => 'email',
      '#title' => $this->t("Your Email Address"),
      '#required' => TRUE,
      '#default_value' => \Drupal::currentUser()->getEmail(),
    ];

    $form['form_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'form-fieldset-wrapper',
      ],
    ];

    $query_type = $form_state->getValue('query_type') ?? ($this->getRequest()->get('type') ?? 'query');
    $trial_type = $form_state->getValue('trial_type') ?? 'sandbox';

    $this->getForm($query_type, $form['form_container']);

    $form['form_container']['actions'] = ['#type' => 'actions'];

    $form['form_container']['actions']['send'] = ($query_type === 'trial' && $trial_type === 'sandbox') ? [
      '#type' => 'link',
      '#title' => $this->t('Go to Sandbox!'),
      '#url' => Url::fromUri(self::SANDBOX_TRIAL_URL.'&email='.\Drupal::currentUser()->getEmail()),
      '#attributes' => ['target' => '_blank', 'class'=> ['button','button--primary']],
    ] : [
      '#type' => 'submit',
      '#value' => $this->t('Send Query'),
      '#attributes' => [
        'class' => [
          'button--primary',
        ],
      ],
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    $query_type = $form_values['query_type'];
    $customer_email = $form_values['user_email'];
    $moContact = new MiniorangeContact($customer_email);

    if ($query_type === 'query') {

      $query = $form_values['query'];
      $response = $moContact->contact($this->getExtendedQuery($query));
      if ($response && $response['code'] === 200) {
        $this->messenger()->addStatus($this->t("Request sent successfully. We will get back to you shortly on %customer_email.", ['%customer_email' => $customer_email]));
      }
      else {
        $msg = $response['message'] ?? " Something went wrong while sending the query. Please reach out to us at " . MiniorangeContact::SUPPORT_EMAIL;
        $this->messenger()->addError($this->t($msg));
      }

    }
    elseif ($query_type === 'trial' || $query_type === 'feature_request') {

      if ($query_type === 'feature_request') {
        $query = $form_values['query'];
        $subject = "Feature Request for Session Management Module - $customer_email";
        $content = [
          'Company' => $this->getRequest()->getSchemeAndHttpHost() . $this->getRequest()->getBasePath(),
          'Customer Email' => $customer_email,
          'Request Type' => 'Feature Request',
          'General Info' => $this->getExtendedQuery(),
          'Feature info' => $query,
        ];
      }
      else {
        $trial_use_case = $form_values['trial_use_case'];

        $subject = "Trial Request for Session Management Module - $customer_email";
        $content = [
          'Company' => $this->getRequest()->getSchemeAndHttpHost() . $this->getRequest()->getBasePath(),
          'Customer Email' => $customer_email,
          'Trial Type' => "On Premise",
          'General Info' => $this->getExtendedQuery(),
          'Use Case' => $trial_use_case,
        ];
      }

      $notify = $moContact->notify($subject, $this->getTrialEmailBody($content));

      if ($notify && $notify['status'] === 'SUCCESS') {
        $this->messenger()->addStatus($this->t("Request sent successfully. We will get back to you shortly on %customer_email.", ['%customer_email' => $customer_email]));
      }
      else {
        $this->messenger()->addError($this->t("Something went wrong while sending the query. Please reach out to us at %support_email", ['%support_email' => MiniorangeContact::SUPPORT_EMAIL]));
      }
    }

    $this->redirect('session_management.config_form')->send();
  }

  /**
   * Return form based on query type.
   */
  protected function getForm(string $query_type, array &$form) {
    return match ($query_type) {
      'query' => $form += $this->queryForm(),
      'trial' => $form += $this->trialForm(),
      'feature_request' => $form += $this->queryForm(TRUE),
      default => $form,
    };
  }

  /**
   * The general query form.
   */
  protected function queryForm($isFeatureRequest = FALSE): array {

    $form = [];

    $form['query'] = [
      '#type' => 'textarea',
      '#title' => $isFeatureRequest ? $this->t('What would you like to add as a new feature?') : $this->t('How can we help?'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * The module premium trial form.
   */
  protected function trialForm(): array {

    $form = [];

    $form['trial_type'] = [
      '#type' => 'radios',
      '#title' => $this->t("Select Your Preferred Testing Option"),
      '#options' => [
        'sandbox' => $this->t("Test on miniOrange Sandbox (Quick Setup)"),
        'on_premise' => $this->t("Test on My Own Environment"),
      ],
      '#default_value' => 'sandbox',
      '#ajax' => [
        'callback' => '::supportCallbackFunction',
        'wrapper' => 'form-fieldset-wrapper',
      ],
    ];

    $form['trial_use_case'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Which features are you interested?'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="trial_type"]' => ['value' => 'on_premise'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Callback function.
   */
  public function supportCallbackFunction(array $form, FormStateInterface $form_state) {
    return $form['form_container'];
  }

  /**
   * Function to create a query.
   */
  public function getExtendedQuery($query = ''): string {

    $server = $_SERVER['SERVER_SOFTWARE'] ?? "";
    $additional_info = "[ Drupal " . \Drupal::VERSION . " | Session Management | " . $server . " ] ";
    return $additional_info . $query;
  }

  /**
   * Function to create a body fo trial email.
   */
  public function getTrialEmailBody($content) {

    $html = '<!DOCTYPE html><html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Email Template</title>
  <style>
    body {
      font-family: Arial, sans-serif;
    }

    table {
      width: 100%;
      border-collapse: collapse;
    }

    th, td {
      padding: 10px;
      text-align: left;
      border-bottom: 1px solid #efefef;
    }

    th {
      background-color: #FAFAFA;
    }

    /* Set width for the first column */
    th:first-child, td:first-child {
      width: 20%;
    }
  </style>
</head>
<body>
  
  <h5>Hello,</h5>
  <table>';

    $info = "";
    foreach ($content as $key => $value) {
      $info .= '<tr><th>' . $key . '</th><td>' . $value . '</td></tr>';
    }

    $html_end = '</table></body></html>';

    return $html . $info . $html_end;
  }

}
