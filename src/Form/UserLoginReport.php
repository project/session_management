<?php

  namespace Drupal\session_management\Form;

  use Drupal\Core\Form\ConfigFormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Url;

  class UserLoginReport extends ConfigFormBase
  {

    public const SETTINGS = 'session_management.settings';

    /**
     * {@inheritDoc}
     */
    protected function getEditableConfigNames() {
      return [static::SETTINGS];
    }

    /**
     * {@inheritDoc}
     */
    public function getFormId() {
      return 'user-login-report';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $premium_tag = '<a href="' . Url::fromRoute('session_management.licensing_form')->toString() . '" target="_blank"><b>[Premium]</b></a>';

      $form['#disabled'] = true;

      $form['note'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This is a session report view. You can modify the below as per your requirements.') ."<br>".$this->t('This feature is available in the ').$premium_tag . $this->t(' version.')
      ];

      // Filter Section
      $form['filters'] = [
        '#type' => 'details',
        '#title' => $this->t('Filters'),
      ];

      // Name Autocomplete
      $form['filters']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#description' => $this->t('Enter a comma-separated list of user names.'),
        '#default_value' => '',
      ];

      // IP Address
      $form['filters']['ip_address'] = [
        '#type' => 'textfield',
        '#title' => $this->t('IP Address'),
        '#default_value' => '',
      ];

      // Sort by
      $form['filters']['sort_by'] = [
        '#type' => 'select',
        '#title' => $this->t('Sort by'),
        '#options' => [
          'name' => $this->t('Name'),
          'ip_address' => $this->t('IP Address'),
          'login' => $this->t('Login'),
        ],
        '#default_value' => 'name',
      ];

      // Order
      $form['filters']['order'] = [
        '#type' => 'radios',
        '#title' => $this->t('Order'),
        '#options' => [
          'asc' => $this->t('Asc'),
          'desc' => $this->t('Desc'),
        ],
        '#default_value' => 'asc',
      ];

      // Submit button
      $form['filters']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Apply Filters'),
      ];

      // Table structure
      $header = [
        $this->t('Name'),
        $this->t('IP Address'),
        $this->t('Browser/Device'),
        $this->t('Login'),
        $this->t('Logout'),
        $this->t('Details'),
      ];

      // Example data for the table
      $rows = [
        ['John Doe', '192.168.1.1', 'Chrome', '08/05/2024 03:05:15 PM', '09/05/2024 01:55:20 AM', 'Details'],
        ['Jane Smith', '192.168.1.2', 'Firefox', '09/05/2024 09:01:00 PM', '09/05/2024 09:08:15 PM', 'Details'],
      ];

      $form['session_report_table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty_text' => $this->t('No records found.'),
      ];

      return $form;
    }

  }