<?php

  namespace Drupal\session_management\Form;

  use Drupal\Core\Form\ConfigFormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Url;

  /**
   * Login setting configuration form.
   */
  class LoginSettingsForm extends ConfigFormBase {

    public const SETTINGS = 'session_management.settings';

    /**
     * {@inheritDoc}
     */
    protected function getEditableConfigNames() {
      return [static::SETTINGS];
    }

    /**
     * {@inheritDoc}
     */
    public function getFormId() {
      return 'login-settings-form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $premium_tag = '<a href="' . Url::fromRoute('session_management.licensing_form')->toString() . '" target="_blank"><b>[Premium]</b></a>';

      $form['libraries'] = [
        '#attached' => [
          'library' => [
            "session_management/session_management.mo_session",
          ],
        ],
      ];

      $form['#disabled'] = TRUE;

      $form['div_start'] = [
        '#type' => 'markup',
        '#markup' => '<div class="mo-table-layout-2"><div class="mo-table-layout-3 mo-container1">',
      ];

      $form['flood_control'] = [
        '#type' => 'details',
        '#title' => $this->t('Flood Control Settings'),
        '#description' => $this->t('Manage settings to prevent users from attempting too many login attempts in a short period of time.'),
      ];

      $form['flood_control']['enable_flood_control'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Flood Control'),
        '#description' => $this->t('Check this box to enable flood control.'),
        '#default_value' => 0, // Default to enabled.
        '#disabled' => false,
      ];

      $form['flood_control']['login_attempts_limit'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Login Attempts Limit'),
        '#description' => $this->t('Set the maximum number of login attempts allowed before triggering flood control.'),
        '#default_value' => '5',
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
        '#attributes' => [
          'placeholder' => 'e.g., 5',
        ],
      ];

      $form['flood_control']['time_window'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Time Window (minutes)'),
        '#description' => $this->t('Set the time window (in minutes) during which the login attempts limit is enforced.'),
        '#default_value' => '15',
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
        '#attributes' => [
          'placeholder' => 'e.g., 15',
        ],
      ];

      $form['flood_control']['block_duration'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Block Duration (minutes)'),
        '#description' => $this->t('Set the duration (in minutes) for which a user will be blocked from logging in after exceeding the login attempts limit.'),
        '#default_value' => '30',
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
        '#attributes' => [
          'placeholder' => 'e.g., 30',
        ],
      ];

      $form['flood_control']['flood_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Custom Error Message'),
        '#description' => $this->t('Enter a custom error message to display when a user is blocked due to too many login attempts.'),
        '#default_value' => $this->t('Access denied due to too many failed login attempts. Please try again later.'),
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
        '#rows' => 1,
      ];

// Admin Bypass
      $form['flood_control']['admin_bypass'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Admin Bypass'),
        '#description' => $this->t('Allow users with administrative roles to bypass flood control.'),
        '#default_value' => 0,
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
      ];

// Notification
      $form['flood_control']['notify_admin'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Notify Admin on Block'),
        '#description' => $this->t('Send a notification to the site administrator when a user is blocked.'),
        '#default_value' => 0,
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
      ];

// IP Whitelisting
      $form['flood_control']['whitelisted_ips'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Whitelisted IP Addresses'),
        '#description' => $this->t('Enter IP addresses that are allowed to bypass flood control, one per line.'),
        '#default_value' => '',
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
        '#rows' => 3,
        '#attributes' => [
          'placeholder' => 'e.g., 192.168.0.1',
        ],
      ];

// Logging
      $form['flood_control']['enable_logging'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Logging'),
        '#description' => $this->t('Log flood control events for review and auditing.'),
        '#default_value' => 0,
        '#states' => [
          'visible' => [
            ':input[name="enable_flood_control"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['login_restriction'] = [
        '#type' => 'details',
        '#title' => $this->t('Login Restriction'),
      ];

      $form['login_restriction']['restriction'] = [
        '#type' => 'vertical_tabs',
      ];

// IP-Based Restriction
      $form['ip_restriction'] = [
        '#type' => 'details',
        '#title' => $this->t('IP-Based Restriction <i>[coming soon]</i>'),
        '#description' => $this->t('Control user access by allowing logins only from specific IP addresses or ranges. This helps ensure that only users from approved locations can access your site.'),
        '#group' => 'restriction',
      ];

      $form['ip_restriction']['ip_range_list'] = [
        '#type' => 'textarea',
        '#title' => $this->t("Allowed IP Ranges"),
        '#description' => '<i>'.$this->t('List of allowed IP addresses or ranges (one per line). Example: 192.168.0.1 or 192.168.0.0/24').'</i>',
        '#rows' => 2,
      ];

      $form['ip_restriction']['ip_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t("Error message"),
        '#description' => $this->t('Error message shown when a user tries to log in from an unauthorized IP address.'),
        '#default_value' => $this->t('Access denied. Your IP address is not authorized to access this site.'),
        '#rows' => 1,
      ];

// Country-Based Restriction
      $form['country_restriction'] = [
        '#type' => 'details',
        '#title' => $this->t('Country-Based Restriction '.$premium_tag),
        '#description' => $this->t('Limit user logins based on their geographical location. Only users from selected countries will be able to log in, providing an extra layer of security.'),
        '#group' => 'restriction',
      ];

      $form['country_restriction']['country_list'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Allowed Countries'),
        '#description' => '<i>'.$this->t('List of allowed countries (one per line). Example: United States').'</i>',
        '#rows' => 2,
      ];

      $form['country_restriction']['country_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t("Error message"),
        '#description' => $this->t('Error message shown when a user tries to log in from a restricted country.'),
        '#default_value' => $this->t('Access denied. Logins from your country are not permitted.'),
        '#rows' => 1,
      ];

// Device-Based Restriction
      $form['device_restriction'] = [
        '#type' => 'details',
        '#title' => $this->t('Device-Based Restriction '.$premium_tag),
        '#description' => $this->t('Manage user access by restricting logins to approved devices. Users can only log in from devices that have been authorized, enhancing site security.'),
        '#group' => 'restriction',
      ];

      $form['device_restriction']['device_list'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Allowed Devices'),
        '#description' => '<i>'.$this->t('List of allowed devices (one per line). Example: iPhone, Windows PC').'</i>',
        '#rows' => 2,
      ];

      $form['device_restriction']['device_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t("Error message"),
        '#description' => $this->t('Error message shown when a user tries to log in from an unauthorized device.'),
        '#default_value' => $this->t('Access denied. Your device is not authorized to access this site.'),
        '#rows' => 1,
      ];


      //Time based access control

      $form['time_based_access_control'] = [
        '#type' => 'details',
        '#title' => t('Time-Based Access Control Settings '.$premium_tag),
      ];

      $form['time_based_access_control']['absolute_access_control'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable Absolute Time-Based Access Control'),
        '#description' => t('Restrict access to resources based on specific start and end dates/times.'),
      ];

      $form['time_based_access_control']['absolute_access_control_start'] = [
        '#type' => 'datetime',
        '#title' => t('Start Date/Time'),
        '#states' => [
          'visible' => [
            ':input[name="absolute_access_control"]' => ['checked' => TRUE],
          ],
        ],
        '#disabled' => true
      ];

      $form['time_based_access_control']['absolute_access_control_end'] = [
        '#type' => 'datetime',
        '#title' => t('End Date/Time'),
        '#states' => [
          'visible' => [
            ':input[name="absolute_access_control"]' => ['checked' => TRUE],
          ],
        ],
        '#disabled' => true
      ];

      $form['time_based_access_control']['periodic_access_control'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable Periodic Time-Based Access Control'),
        '#description' => t('Set access rules based on recurring schedules (e.g., daily, weekly).'),
        '#default_value' => 1,
        '#disabled' => false
      ];

      $form['time_based_access_control']['periodic_access_control_frequency'] = [
        '#type' => 'select',
        '#title' => t('Schedule Frequency'),
        '#options' => [
          'daily' => t('Daily'),
          'weekly' => t('Weekly'),
          'monthly' => t('Monthly'),
        ],
        '#states' => [
          'visible' => [
            ':input[name="periodic_access_control"]' => ['checked' => TRUE],
          ],
        ],
        '#disabled' => false
      ];

      $form['time_based_access_control']['periodic_access_control_time_range'] = [
        '#type' => 'fieldset',
        '#title' => t('Time Range'),
        '#states' => [
          'visible' => [
            ':input[name="periodic_access_control"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['time_based_access_control']['periodic_access_control_time_range']['periodic_access_control_start'] = [
        '#type' => 'datetime',
        '#title' => t('Start Date/Time'),
        '#states' => [
          'visible' => [
            ':input[name="periodic_access_control"]' => ['checked' => TRUE],
          ],
        ],
        '#disabled' => true
      ];

      $form['time_based_access_control']['periodic_access_control_time_range']['periodic_access_control_end'] = [
        '#type' => 'datetime',
        '#title' => t('End Date/Time'),
        '#states' => [
          'visible' => [
            ':input[name="periodic_access_control"]' => ['checked' => TRUE],
          ],
        ],
        '#disabled' => true
      ];


      $form['time_based_access_control']['periodic_access_control_time_range']['start_time'] = [
        '#type' => 'time',
        '#title' => t('Start Time'),
      ];

      $form['time_based_access_control']['periodic_access_control_time_range']['end_time'] = [
        '#type' => 'time',
        '#title' => t('End Time'),
      ];

      $form['time_based_access_control']['periodic_access_control_days'] = [
        '#type' => 'checkboxes',
        '#title' => t('Days of the Week'),
        '#options' => [
          'monday' => t('Monday'),
          'tuesday' => t('Tuesday'),
          'wednesday' => t('Wednesday'),
          'thursday' => t('Thursday'),
          'friday' => t('Friday'),
          'saturday' => t('Saturday'),
          'sunday' => t('Sunday'),
        ],
        '#states' => [
          'visible' => [
            ':input[name="periodic_access_control_frequency"]' => ['value' => 'weekly'],
          ],
        ],
        '#disabled' => true
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save Configuration'),
        '#button_type' => 'primary',
      ];

      $form['div_end'] = [
        '#type' => 'markup',
        '#markup' => '</div></div>',
      ];

      return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
    }

  }
