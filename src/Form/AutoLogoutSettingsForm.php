<?php

  namespace Drupal\session_management\Form;

  use Drupal\Core\Form\ConfigFormBase;
  use Drupal\Core\Form\FormStateInterface;
  use Drupal\Core\Url;

  /**
   * Configuration class for saving auto logout feature configuration.
   */
  class AutoLogoutSettingsForm extends ConfigFormBase {

    public const SETTINGS = 'session_management.settings';

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
      return [static::SETTINGS];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'autologout-settings-form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

      $config = $this->config(static::SETTINGS);
      $premium_tag = '<a href="' . Url::fromRoute('session_management.licensing_form')->toString() . '" target="_blank"><b>[Premium]</b></a>';

      $form['libraries'] = [
        '#attached' => [
          'library' => [
            "session_management/session_management.mo_session",
          ],
        ],
      ];

      $form['#disabled'] = FALSE;

      $form['div_start'] = [
        '#type' => 'markup',
        '#markup' => '<div class="mo-table-layout-2"><div class="mo-table-layout-3 mo-container1">',
      ];

      $form['browser_session_destroy'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable user session destroy '.$premium_tag),
        '#description' => $this->t('Enable this if you want to terminate the user session after the browser is closed.'),
        '#disabled' => TRUE,
      ];

      $form['logout_password_change'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Logout users on password change '.$premium_tag),
        '#description' => $this->t('Immediately end all active sessions when a user changes their password to enhance security.'),
        '#disabled' => TRUE,
      ];

      $form['auto_logout_detail'] = [
        '#type' => 'details',
        '#title' => t('User Auto Logout Settings'),
        '#open' => TRUE,
      ];

      $form['auto_logout_detail']['autologout_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Autologout'),
        '#description' => $this->t('Enable autologout on the site.'),
        '#default_value' => $config->get('autologout_enabled'),
      ];

      $form['auto_logout_detail']['exclude_admin_autologout'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Exclude admin from autologout '.$premium_tag),
        '#description' => $this->t('Exclude users with administrative roles to bypass auto logout.'),
        '#disabled' => true,
      ];

      $form['auto_logout_detail']['force_logout'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Force User Logout '.$premium_tag),
        '#description' => $this->t('Logout the user regardless of their activity.'),
        '#default_value' => false,
      ];

      $form['auto_logout_detail']['fl_fieldset'] = [
        '#type' => 'fieldset',
        '#states' => [
          'visible' => [
            ':input[name = "force_logout"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['auto_logout_detail']['fl_fieldset']['force_logout_time'] = [
        '#type' => 'number',
        '#title' => $this->t('Force User Logout Time '.$premium_tag),
        '#description' => $this->t('Time in seconds after which the user will be forcefully logged out.'),
        '#min' => '30',
        '#disabled' => true,
      ];

      $form['auto_logout_detail']['fl_fieldset']['show_logout_timer'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Display User Logout Timer '.$premium_tag),
        '#description' => $this->t("Display a user logout clock or countdown timer (like a stopwatch) for the logged-in user's session"),
        '#disabled' => true,
      ];

      $form['auto_logout_detail']['autologout_timeout'] = [
        '#type' => 'number',
        '#title' => $this->t('Logout Timeout (in seconds)'),
        '#description' => $this->t('The duration of user inactivity (in seconds), that triggers the display of a logout notification dialog.'),
        '#min' => '50',
        '#default_value' => $config->get('autologout_timeout') ?? 50,
      ];

      $form['auto_logout_detail']['autologout_response_time'] = [
        '#type' => 'number',
        '#title' => $this->t('Response Time (in seconds)'),
        '#description' => $this->t('The time allowed for user response to the logout dialog box'),
        '#min' => '5',
        '#default_value' => $config->get('autologout_response_time') ?? 5,
      ];

      $form['auto_logout_detail']['modal_info'] = [
        '#type' => 'details',
        '#title' => $this->t('Modal Dialog Info'),
      ];

      $form['auto_logout_detail']['modal_info']['mo_modal_width'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Modal Custom width'),
        '#size' => 40,
        '#description' => $this->t('Enter the width of the modal dialog in pixels.'),
        '#default_value' => $config->get('mo_modal_width') ?? 400,
      ];

      $form['auto_logout_detail']['modal_info']['mo_modal_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Modal Custom Title'),
        '#required' => TRUE,
        '#default_value' => $config->get('mo_modal_title') ?? \Drupal::config('system.site')->get('name') . ' Alert',
        '#size' => 40,
        '#description' => $this->t('Enter the width of the modal dialog in pixels.'),
      ];

      $form['auto_logout_detail']['modal_info']['mo_modal_message'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Modal Custom Message'),
        '#required' => TRUE,
        '#default_value' => $config->get('mo_modal_message') ?? "You've been inactive for a while. Would you like to continue your session?",
        '#size' => 40,
        '#description' => $this->t('Enter the message to display in the logout modal dialog.'),
        '#rows' => 1
      ];

      $form['auto_logout_detail']['modal_info']['mo_modal_yes_button_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Modal Custom Confirm button text'),
        '#required' => TRUE,
        '#default_value' => $config->get('mo_modal_yes_button_text') ?? "Accept",
        '#size' => 40,
        '#description' => $this->t('Enter the confirmation button text to <b>extend</b> the user session.'),
      ];

      $form['auto_logout_detail']['modal_info']['mo_modal_no_button_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Modal Custom Decline button text'),
        '#required' => TRUE,
        '#default_value' => $config->get('mo_modal_no_button_text') ??  "Deny",
        '#size' => 40,
        '#description' => $this->t('Enter the confirmation button text to <b>end</b> the user session.'),
      ];

      $form['auto_logout_detail']['rb_logout_detail'] = [
        '#type' => 'details',
        '#title' => $this->t('Role Based Logout ').$premium_tag,
        '#description' => $this->t('Configure the auto logout timer for the user based on their roles.'),
        '#open' => FALSE,
      ];

      $form['auto_logout_detail']['rb_logout_detail']['role_based_logout'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable role based logout'),
        '#disabled' => TRUE
      ];

      $rows = [
        [
          'roles' => [
            'data' => [
              '#type' => 'select',
              '#options' => $this->getRolesList(),
            ],
          ],
          'logout_time' => [
            'data' => [
              '#type' => 'number',
              '#min' => '5',
              '#default_value' => 50,
            ],
          ],
        ],
      ];

      $form['auto_logout_detail']['rb_logout_detail']['role_based_logout_table'] = [
        '#type' => 'table',
        '#header' => [
          'roles' => $this->t('Select Role') ,
          'logout_time' => $this->t('Logout Time'),
        ],
        '#rows' => $rows,
        '#disabled' => TRUE
      ];

      $form['auto_logout_detail']['rb_logout_detail']['add_rows'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Add row +'),
        '#disabled' => TRUE
      ];

      $form['auto_logout_detail']['redirect_after_logout'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Redirect After Auto-logout '.$premium_tag),
        '#disabled' => TRUE,
        '#attributes' => [
          'placeholder' => $this->t('eg; https://example.com/node1')
        ]
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save Configuration'),
        '#button_type' => 'primary',
      ];

      $form['div_end'] = [
        '#type' => 'markup',
        '#markup' => '</div></div>',
      ];

      return $form;
    }

    public function getRolesList(): array
    {
      $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
      $option = [];

      foreach ($roles as $role) {
        $option[$role->getOriginalId()] = $role->label();
      }
      unset($option['anonymous'], $option['authenticated']);

      return $option;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
      $form_values = $form_state->getValues();
      $config = $this->configFactory()->getEditable(self::SETTINGS);

      $fields = [
        'autologout_enabled',
        'autologout_timeout',
        'autologout_response_time',
        'mo_modal_width',
        'mo_modal_title',
        'mo_modal_message',
        'mo_modal_yes_button_text',
        'mo_modal_no_button_text',
        'redirect_after_logout',
      ];
      foreach ($fields as $field) {
        $config->set($field, $form_values[$field]);
      }
      $config->save();

      $this->messenger()->addStatus(t("Configuration saved successfully."));

    }

  }
